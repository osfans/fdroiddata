Merk: F-Droid krever root-tilgang for å installere F-Droid-rettighetsutvidelse som systemisk "priv-program".

<a href="https://f-droid.org/packages/org.fdroid.fdroid">F-Droid</a> kan gjøre bruk av systemprivilegier for å installere, oppdatere og fjerne programmer på egenhånd. Den eneste måten å oppnå de privilegiene på er å være et systemprogram.

Dette er der F-Droid-rettighetsutvidelsen gjør sin inntreden - i det å være et eget program som er mye mindre, kan det installeres som et systemprogram og kommunisere med hovedprogrammet via AIDL IPC.

Dette har flere fordeler:

* Redusert diskbruk i systempartisjon * Systemoppdateringer fjerner ikke F-Droid * Prosessen med å installere i system via root er tryggere

Instedenfor denne byggversjonen, vil de fleste brukere kunne installere den "luftbårne" (OTA)-oppdaterings-ZIP-fila kalt <a href="https://f-droid.org/packages/org.fdroid.fdroid.privileged.ota">F-Droid Privileged Extension OTA</a>. Dette er her for å oppgradere F-Droid-rettighetsutvidelsen når den har blitt installert ved bruk av OTA ZIP.

