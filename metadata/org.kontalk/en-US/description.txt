<b>Follow @kontalknet on Twitter or identi.ca to keep up to date with the latest
changes and on service status!</b>

Kontalk is a secure instant messenger, allowing you to send and receive text,
image and voice messages (other file types coming soon) to and from other
Kontalk users completely free of charge (*).

* <b>Your phone number is your ID.</b> No usernames or passwords
* <b>It automatically finds other Kontalk users</b> by looking at your address book, making it even easier to start chatting on Kontalk with your friends
* <b>End-to-end encryption</b> ensures safe and private conversations, so that only you and the person you are talking to can read them
* Your and all phone numbers used on the network are irreversibly encrypted
* It supports multiple devices, making it very easy to start chatting on your phone and continue the conversation on your tablet
* Based on open standards: XMPP and OpenPGP

The network relies on community donations to keep the infrastructure running, so
if you can, please donate on <a href="http://kontalk.net/">kontalk.net</a>. Even small
donations matter.

You can find all permissions used by the app explained at
[https://github.com/kontalk/androidclient/wiki/Android-client-permissions this
page].

This version has been stripped of all Google components, meaning no support for
push notifications.

(*) Carrier fees for Internet traffic may apply
